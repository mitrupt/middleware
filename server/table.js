const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/table', (request, response) => {
    const connection = db.connect();
    const statement = `select id,status from tables`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/table/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `select status from tables where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/table', (request, response) => {
    const {status} = request.body;
    const connection = db.connect();
    const statement = `insert into tables
            (status) values 
            ('${status}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/table/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `delete from tables where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
module.exports = router;
