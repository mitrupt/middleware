const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/orders', (request, response) => {
    const connection = db.connect();
    const statement = `select order_id, item_id, table_id, order_in_time, order_out_time, order_status from orders`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/orders/:order_id', (request, response) => {
    const order_id = request.params.order_id;
    const connection = db.connect();
    const statement = `select item_id, table_id, order_in_time, order_out_time, order_status from orders where order_id = ${order_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/orders', (request, response) => {
    const { item_id, table_id, order_in_time, order_out_time, order_status } = request.body;
    const connection = db.connect();
    const statement = `insert into orders
            ( item_id, table_id, order_in_time, order_out_time, order_status ) values 
            ('${item_id}, '${table_id}', '${order_in_time}','${order_out_time}','${order_status}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/orders/:order_id', (request, response) => {
    const order_id = request.params.order_id;
    const connection = db.connect();
    const statement = `delete from orders where orders_id = ${order_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
module.exports = router;
