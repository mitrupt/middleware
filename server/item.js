const express = require('express');
const db = require('./db');
const utils = require('./utils');
const multer = require('multer');
const upload = multer({dest: 'images/'});

const router = express.Router();

router.get('/item', (request, response) => {
    const connection = db.connect();
    const statement = `select item_id,item_name, thumbnail,category_id,price from items`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/item/:category_id', (request, response) => {
    const category_id = request.params.category_id;
    const connection = db.connect();
    const statement = `select i.item_id, i.item_name, i.price, i.thumbnail from items i inner join category c on c.category_id = i.category_id where i.category_id = ${category_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/item', upload.single('thumbnail'),(request, response) => {
    const {item_name , thumbnail, category_id, price} = request.body;
    const connection = db.connect();
    const statement = `insert into items
            ( item_name , category_id, thumbnail, price) values 
            ('${item_name}','${category_id}', '${thumbnail}', '${price}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/item/:item_id', (request, response) => {
    const item_id = request.params.item_id;
    const connection = db.connect();
    const statement = `delete from items where item_id = ${item_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
module.exports = router;
