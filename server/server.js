const express = require('express');
const bodyParser = require('body-parser');
const categoryRouter = require('./category');
const itemRouter = require('./item');
const tableRouter = require('./table');
const ordersRouter = require('./orders');
const userRouter = require('./user');


const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static('images'));


app.use(categoryRouter);
app.use(itemRouter);
app.use(tableRouter);
app.use(ordersRouter);
app.use(userRouter);

app.listen(3000,'0.0.0.0',()=>{
    console.log('server started on port 3000');
})
