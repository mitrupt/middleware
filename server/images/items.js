const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/category', (request, response) => {
    const connection = db.connect();
    const statement = `select category_id,category_name, thumbnail from category`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/category/:category_id', (request, response) => {
    const category_id = request.params.category_id;
    const connection = db.connect();
    const statement = `select category_name, thumbnail from category where category_id = ${category_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/category', (request, response) => {
    const {category_name , thumbnail} = request.body;
    const connection = db.connect();
    const statement = `insert into category
            ( category_name , thumbnail ) values 
            ('${category_name}', '${thumbnail}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/category/:category_id', (request, response) => {
    const category_id = request.params.category_id;
    const connection = db.connect();
    const statement = `delete from category where category_id = ${category_id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
module.exports = router;
