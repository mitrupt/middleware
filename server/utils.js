function createResponse(error, result){
    let status = '';
    data = null;

    if(error == null){
        status = 'success';
        data = result;
    }else{
        status = 'error';
        data = error;
    }

    return{
        status: status,
        data: data
    };

}
module.exports = {
    createResponse: createResponse
};